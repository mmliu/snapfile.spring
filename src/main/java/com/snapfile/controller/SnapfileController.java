package com.snapfile.controller;

import com.snapfile.factory.FileCodeFactory;
import com.snapfile.util.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Created by mmliu on 02/10/2016.
 * Copied by mmliu on 09/08/2017.
 */
@RestController
public class SnapfileController {
    Log log = LogFactory.getLog(getClass());

    final String FileDir = "./upload/";
    final String Domain = "http://snapfilex.com/";

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public String in(@RequestParam("file") MultipartFile file){
        try {
            String originalName = file.getOriginalFilename();
            Pair<String, String> pair = FileCodeFactory.genCodeAndFormatedFileNameInDir(originalName, FileDir);

            Files.copy(file.getInputStream(), Paths.get(FileDir).resolve(pair.v));

            String url2Get = Domain + pair.k;

            String respInJson = "{\"desc\":\"%s\"}";
            return String.format(respInJson, "succeed, use this link to get the file: " + url2Get);
        } catch (IOException e) {
            e.printStackTrace();

            return "fail";
        }
    }

    @RequestMapping(value = "/{code}", method = RequestMethod.GET)
    public void get(@PathVariable String code,
                    HttpServletResponse response){
        log.debug("file code:" + code);

        String filename = FileCodeFactory.getFileByCode(code, FileDir);
        File file = new File(FileDir + filename);

        if(!StringUtils.isEmpty(filename) && file.exists()) {
            try {
                //todo response.setContentType();
                String originalName = FileCodeFactory.getOriginalFileNameByFormatted(file.getName());

                String contentDisposition = String.format("attachment; filename*=utf8''%s",
                        URLEncoder.encode(originalName, "UTF-8"));

                response.setHeader(HttpHeaders.CONTENT_DISPOSITION, contentDisposition);

                InputStream is = new FileInputStream(file);

                FileCopyUtils.copy(is, response.getOutputStream());

                file.delete();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            response.setStatus(HttpServletResponse.SC_OK);

            try {
                response.getWriter().write("File Not Exist");
                response.getWriter().flush();
                response.getWriter().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Created by mmliu on 09/08/2017.
     */
    //@RestController
    public static class DeprecatedController {

        @RequestMapping("/send")
        public String send(@RequestParam("file") MultipartFile file){
            try {
                save(file);
            }catch(Exception e){

            }

            return "file saved";
        }

        @RequestMapping("/get")
        public String get(@RequestParam String fileName, HttpServletResponse response){
            String fileDir = "./upload/";
            File uploadDir = new File(fileDir);

            String[] fileNames = uploadDir.list();
            boolean exist = Arrays.stream(fileNames).anyMatch(n -> fileName.equals(n));

            if(exist){
                try {
                    File file2Send = new File(fileDir + fileName);

                    InputStream is = new FileInputStream(file2Send);
                    FileCopyUtils.copy(is, response.getOutputStream());

                    response.flushBuffer();

                    file2Send.delete();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return "exist";
            }else{
                return "not exist";
            }
        }

        private void save(MultipartFile file) throws Exception {
            String fileDir = "./upload/";
            File uploadDir = new File(fileDir);
            if (!uploadDir.exists()) {
                try {
                    uploadDir.mkdir();
                } catch (Exception e) {
                    throw new Exception("内部错误，请稍后再试");
                }
            }

            String filename = fileDir + file.getOriginalFilename();
            File savedFile = new File(filename);
            //this.logger.info("文件的绝对路径：" + savedFile.getAbsolutePath());
            //this.logger.info("类文件的绝对路径：" + ClassUtils.getDefaultClassLoader().getResource("").getPath());

            saveFile2Server(file, savedFile);
        }

        private String saveFile2Server(MultipartFile file, File savedFile) {
            try {
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(savedFile));
                out.write(file.getBytes());
                out.flush();
                out.close();

                return "文件上传到服务器成功!";
            } catch (Exception e) {
                e.printStackTrace();
                return "文件上传到服务器失败!";
            }
        }
    }
}

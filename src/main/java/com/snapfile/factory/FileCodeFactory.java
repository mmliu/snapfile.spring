package com.snapfile.factory;

import com.snapfile.util.Pair;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.Arrays;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class FileCodeFactory {
    static String Seperator = "6SNAPFILE6";

    public static Pair<String, String> genCodeAndFormatedFileNameInDir(String filename, String fileDir) {
        String code = genCode(fileDir);
        String newFileName = String.format("%s%s%s", code, Seperator, filename);

        return new Pair<>(code, newFileName);
    }

    private static String genCode(String fileDir) {
        File file = new File(fileDir);

        String[] names = file.list();

        Set<String> codes = Arrays.stream(names).map(name -> {
            String[] pairs = name.split(Seperator);

            if(pairs.length >= 2){
               return pairs[0];
            }

            return "";
        }).collect(Collectors.toSet());

        String rstCode = randomCode();
        while(codes.contains(rstCode)){
            rstCode = randomCode();
        }

        return rstCode;
    }

    private static String randomCode(){
        Random random = new Random();

        Integer code = random.nextInt(10000);

        return code.toString();
    }

    public static String getFileByCode(String code, String fileDir) {
        if(StringUtils.isEmpty(code) || StringUtils.isEmpty(fileDir)){
            return null;
        }

        File file = new File(fileDir);

        String[] names = file.list();

        for(String name : names){
            String[] pairs = name.split(Seperator);

            if(pairs.length >= 2 && pairs[0].equals(code)){
                return name;
            }
        }

        return null;
    }

    public static String getOriginalFileNameByFormatted(String formatedFileName) {
        if(StringUtils.isEmpty(formatedFileName)){
            return null;
        }

        String[] pairs = formatedFileName.split(Seperator);

        if(pairs.length < 2){
            return null;
        }

        return pairs[1];
    }
}

FROM java:8-jre
MAINTAINER Liumingmin <diveintotomorrow@gmail.com>

ADD ./build/libs/snapfile.spring-0.0.1-SNAPSHOT.jar .

VOLUME /upload

EXPOSE 8081

CMD ["java", "-jar", "snapfile.spring-0.0.1-SNAPSHOT.jar"]

# SnapfileX

## Docker 

### build image

docker build -t diveinto/snapfile.spring:latest .

### push to repo docker.io

docker push diveinto/snapfile.spring:latest

### pull image

docker pull diveinto/snapfile.spring

### update and run

docker stop {{running image}}  

docker run -d -p 8081:8081 diveinto/snapfile.spring
